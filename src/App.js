import React, { Component } from 'react'
import {Switch,Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from "./components/Navbar";
import ProductList from "./components/ProductList";
import Details from "./components/Details";
import Cart from "./components/Cart";
import Default from "./components/Default";



function App () {
  
    return (
      <React.Fragment>
        <Navbar/>
        <Switch>
            <Route exact path='/' componet={ProductList}/>
            <Route path='/details' componet={Details}/>
            <Route path='/cart' componet={Cart}/>
            <Route componet={Default}/>
            <Cart/>
        </Switch>
        <ProductList/>
      </React.Fragment>
    )

}
 export default App;